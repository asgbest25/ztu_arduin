int ledPin = 13;
int inPin = 7;
boolean val = 0;



void setup() {
 pinMode(ledPin, OUTPUT);
 pinMode(inPin, INPUT);

}

void loop() {
 val = digitalRead(inPin);
 if (val==1)
  for (int i=0; i<4; i++)
  {
    digitalWrite(ledPin, HIGH);
    delay(50);
    digitalWrite(ledPin, LOW);
    delay(200);
  }
 
}

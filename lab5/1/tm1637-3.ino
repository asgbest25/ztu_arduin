#include "TM1637.h"

void setup() {
  TM1637Init();
}

void loop() {
  uint8_t full[] = { 0xff, 0xff, 0xff, 0xff };
  uint8_t data[] = { 0, 0b00000110, 0b01011011, 0b01001111 };
  uint8_t blank[] = { 0x00, 0x00, 0x00, 0x00 };
  TM1637SetBrightness(0x0f);

  TM1637SetSegments(data);
  delay(2000);

  TM1637SetSegments(blank);
  delay(1000);

  TM1637SetSegments(full);
  delay(2000);

  TM1637Clear();
  delay(1000);

}

#include "TM1637.h"

void setup() {
  TM1637Init();
}

void loop() {
  uint8_t dog[] = {0, 0b01011110, 0b00111111, 0b01111101};
  uint8_t salo[] = {0b00111001, 0b01110111, 0b00110111, 0b00111111 };
  uint8_t grad[] = {0b01000000, 0b01011011, 0b01101101, 0b01100011 };
  uint8_t blank[] = { 0x00, 0x00, 0x00, 0x00 };
  TM1637SetBrightness(0x0f);

  TM1637SetSegments(salo);
  delay(2000);

  TM1637SetSegments(blank);
  delay(1000);

  TM1637SetSegments(dog);
  delay(2000);

  TM1637SetSegments(grad);
  delay(2000);

  TM1637Clear();
  delay(1000);

}

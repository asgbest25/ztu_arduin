extern "C" {
  #include <stdlib.h>
  #include <string.h>
  #include <inttypes.h>
}

#include <Arduino.h>

#define TM1637_I2C_COMM1    0x40
#define TM1637_I2C_COMM2    0xC0
#define TM1637_I2C_COMM3    0x80

//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     --- . H
//      D

uint8_t m_pinClk = 2;
uint8_t m_pinDIO = 3;
uint8_t m_brightness;
unsigned m_bitDelay = 100;

void TM1637Init(uint8_t pinClk = 2, uint8_t pinDIO = 3, unsigned bitDelay = 100)
{
	m_pinClk = pinClk;
	m_pinDIO = pinDIO;
	m_bitDelay = bitDelay;

	pinMode(m_pinClk, INPUT);
	pinMode(m_pinDIO,INPUT);
	digitalWrite(m_pinClk, LOW);
	digitalWrite(m_pinDIO, LOW);
}

void TM1637BitDelay()
{
  delayMicroseconds(m_bitDelay);
}

void TM1637Start()
{
  pinMode(m_pinDIO, OUTPUT);
  TM1637BitDelay();
}

void TM1637Stop()
{
  pinMode(m_pinDIO, OUTPUT);
  TM1637BitDelay();
  pinMode(m_pinClk, INPUT);
  TM1637BitDelay();
  pinMode(m_pinDIO, INPUT);
  TM1637BitDelay();
}

bool TM1637WriteByte(uint8_t b)
{
  uint8_t data = b;

  // 8 бит данные
  for(uint8_t i = 0; i < 8; i++) {
    // CLK=0
    pinMode(m_pinClk, OUTPUT);
    TM1637BitDelay();

  // Передача бит
    if (data & 0x01)
      pinMode(m_pinDIO, INPUT);
    else
      pinMode(m_pinDIO, OUTPUT);

    TM1637BitDelay();

  // CLK=1
    pinMode(m_pinClk, INPUT);
    TM1637BitDelay();
    data = data >> 1;
  }

  // ACK ожидание
  pinMode(m_pinClk, OUTPUT);
  pinMode(m_pinDIO, INPUT);
  TM1637BitDelay();

  // CLK=1
  pinMode(m_pinClk, INPUT);
  TM1637BitDelay();
  uint8_t ack = digitalRead(m_pinDIO);
  if (ack == 0)
    pinMode(m_pinDIO, OUTPUT);


  TM1637BitDelay();
  pinMode(m_pinClk, OUTPUT);
  TM1637BitDelay();

  return ack;
}

TM1637SetBrightness(uint8_t brightness, bool on = true)
{
	m_brightness = (brightness & 0x7) | (on? 0x08 : 0x00);
}

void TM1637SetSegments(const uint8_t segments[], uint8_t length = 4, uint8_t pos = 0)
{
        // COMM1
	TM1637Start();
	TM1637WriteByte(TM1637_I2C_COMM1);
	TM1637Stop();

	// COMM2 (позиция)
	TM1637Start();
	TM1637WriteByte(TM1637_I2C_COMM2 + (pos & 0x03));

	// Передача данных
	for (uint8_t k=0; k < length; k++)
	  TM1637WriteByte(segments[k]);

	TM1637Stop();

	// COMM3 (яркость)
	TM1637Start();
	TM1637WriteByte(TM1637_I2C_COMM3 + (m_brightness & 0x0f));
	TM1637Stop();
}

void TM1637Clear()
{
	uint8_t data[] = { 0, 0, 0, 0 };
	TM1637SetSegments(data);
}


#define led 13

#include "TM1637.h"

uint8_t numbers[] = { 0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f};
byte minutes = 0;
byte seconds = 0;

void setup() {
  TM1637Init();
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}


void ReadTime(){
  int input = 0;
  int count = Serial.available();
  delay(50);
  for(int i = count; i>0; i--){
    input += (Serial.read()-48) * pow(10, i-1);
  }
  //Serial.println(input);
  minutes = input / 60;
  seconds = input % 60;
  
}

void loop() {

  digitalWrite(led, LOW);

  TM1637SetBrightness(0x0f);
  uint8_t data[] = {0, 0, 0, 0};


  data[0] = numbers[minutes / 10] | 0x80;
  data[1] = numbers[minutes % 10] | 0x80;
  data[2] = numbers[seconds / 10] | 0x80;
  data[3] = numbers[seconds % 10] | 0x80;
  TM1637SetSegments(data);


  if (seconds == 0) {
    if (minutes > 0) {
      minutes--;
      seconds = 59;
    } else {
      digitalWrite(led, HIGH);
      ReadTime();
    }
  }else {
    seconds--;
  }

  delay(1000);

}

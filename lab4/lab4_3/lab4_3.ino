int serv = 9;
int sensorPin = A0; 
float sensorValue = 0;
float sensorValuePrev = 0;
int grad = 0;

void servo (int g){
  g = (g * 10) + 544;
  digitalWrite(serv, HIGH);
  delayMicroseconds(g);
  digitalWrite(serv, LOW);
  delayMicroseconds(20000 - g);
  delay(15);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(serv, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue = analogRead(sensorPin);
  grad = (sensorValue / 1023) * 180;
  servo(grad);
}

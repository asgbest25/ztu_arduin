int sensorPin = A0; 
int sensorValue= 0;

int leds[10] = {3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

void led_ind (float i){
  for (int j = 0; j < 10; j++){
    digitalWrite(leds[j], LOW);
  }
  int res = (i / 1023) * 10;
  for (int j = 0; j < res; j++){
    digitalWrite(leds[j], HIGH);
  }
}

void setup() {
  Serial.begin(9600); 
  for (int j = 0; j < 10; j++){
    pinMode(leds[j], OUTPUT);
  }
}
void loop () {
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  led_ind(sensorValue); 
  delay(100);  
}

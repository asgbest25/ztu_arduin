int leds[10] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

void led_ind (float i){
  for (int j = 0; j < 10; j++){
    digitalWrite(leds[j], LOW);
  }
  int res = (i / 255) * 10;
  for (int j = 0; j < res; j++){
    digitalWrite(leds[j], HIGH);
  }
  if (res <= 3) {
  delay(3000);
  for (int j = 0; j < res; j++){
    digitalWrite(leds[j], LOW);
  }
  }
}

void setup() {
  // put your setup code here, to run once:
  for (int j = 0; j < 10; j++){
    pinMode(leds[j], OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  led_ind(200);
  delay(500);
}
